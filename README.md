Desarrollo del desafío. por Camilo Pino.

En el presente proyecto se encontrará el desarrollo del desafío hasta el punto 4 mencionado en el mismo.

Tras copiar el proyecto y leer instrucciones como recomendaciones previas se comenzó con la construcción 
de un dockerfile que cumpla con las espectativas del solicitante, en este caso se comenzó con una imagen
de node completa pero a su vez demasiado pesada(1,5gb) se tomó la decisión de configurar una "pre-imagen"
en la utiliza node alpine como base a la cual se le agrega curl y bash para poder instalar prune y
utilizarlo para eliminar dependencias que no se usen para la imagen final, posteriormente se elimina de 
manera manual algunas dependencias que no son alcanzadas por el paso anterior y finalmente se crea la imagen 
final copiando el archivo index.js y las dependencias de /node_modules y expone a través del puerto 3000,
todo accesando como el usuario no-root "node".

Para el siguiente paso se definió un nuevo dockerfile que crea una nueva imagen nginx para configurar el proxy reverso y
el acceso a la app nodejs y configuró el acceso con las credenciales "testuser" y "testpassword". 
Se habilitó https y redirigió el tráfico según lo solicitado. Posteriormente generando el Dockercompose que 
administrará ambos servicios(cada uno en su respectivo contenedor) y se expuso nginx por el puerto 80.

Durante el desarrollo de la solución a medida que se fueron realizando los pasos descritos en este 
documento se fue creando el archivo .gitlab-ci.yml en el directorio raíz para realizar las pruebas necesarias
en el apartado ci/cd de gitlab, los dos primeros job están avocados a compilar y probar el funcionamiento de
la aplicación entregada mientras que los ultimos dos se encargan de publicar la imagen docker contenida en 
el contenedor generado en los primeros pasos, valga la redundancia, y realizar el despliegue de Kubernetes 
solicitado posteriormente en el punto 4, luego de esto se realizó la configuración del despliegue y el escalamiento
presente en los documentos en la carpeta k8s. 

