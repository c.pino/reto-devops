#Elección de imagen base para trabajar
FROM node:12-alpine AS BUILD_IMAGE

RUN apk update && apk add curl bash && rm -rf /var/cache/apk/*

#instalar node-prune 
RUN curl -sfL https://install.goreleaser.com/github.com/tj/node-prune.sh | bash -s -- -b /usr/local/bin

#Definir workspace
WORKDIR /app

COPY package.json .

#instala dependencias
RUN npm install

COPY . .

#elimina dependencias
RUN npm prune --production

# correr node prune
RUN /usr/local/bin/node-prune

# Elimina dependencias no utilizadas de npm en linea 15
RUN rm -rf node_modules/rxjs/src/
RUN rm -rf node_modules/rxjs/bundles/
RUN rm -rf node_modules/rxjs/_esm5/
RUN rm -rf node_modules/rxjs/_esm2015/
RUN rm -rf node_modules/swagger-ui-dist/*.map
RUN rm -rf node_modules/couchbase/src/

FROM node:12-alpine

WORKDIR /app

# copia de la pre-imagen build image
COPY --from=BUILD_IMAGE /app/index.js .
COPY --from=BUILD_IMAGE /app/node_modules ./node_modules

EXPOSE 3000

USER node
CMD ["node", "index.js"]